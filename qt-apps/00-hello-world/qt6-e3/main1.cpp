#include <QCoreApplication>
#include <iostream>
#include <string>

void do_cpp()
{
    std::string name;
    std::cout << "Please enter your full name: ";
    std::getline(std::cin, name);
    std::cout << "Hello, " << name << '\n';
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    do_cpp();

    return a.exec();
}
