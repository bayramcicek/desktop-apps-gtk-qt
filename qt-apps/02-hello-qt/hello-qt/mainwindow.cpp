#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    color = 0;
    ui->colorButton->setStyleSheet("QPushButton {background-color: orange;}\n");
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_colorButton_clicked()
{
    QString colors[4] = {"orange", "yellow", "green", "red"};
    color = (color + 1) & 0x3;

    ui->colorButton->setStyleSheet("QPushButton {background-color: " + colors[color] + ";}\n");
}

