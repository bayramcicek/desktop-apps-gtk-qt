#!/bin/bash

BIN=part1-bin
CFILES="part1.c"

gcc -Wno-format -o ${BIN} ${CFILES} \
-Wno-deprecated-declarations -Wno-format-security -lm \
`pkg-config --cflags --libs gtk+-3.0` \
-export-dynamic