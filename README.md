Desktop applications I made with `GTK3/Glade` and `Qt6`

- folder `./gtk3-glade-apps` contains `GTK3` apps
- folder `./qt-apps` contains `Qt6` apps

---

![gtk-glade-hello-world.png](./img/gtk-glade-hello-world.png "gtk")

![qt-hello-world.png](./img/qt-hello-world.png "qt")
